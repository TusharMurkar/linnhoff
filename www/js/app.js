// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers'])

    .run(function ($ionicPlatform, $location) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }


        });

    })
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'loginCtrl',
            })
    
            .state('actions', {
                url: '/actions',
                templateUrl: 'templates/actions.html',
                controller: 'actionsCtrl',
            })

            .state('client-listing', {
                url: '/client-listing',
                templateUrl: 'templates/client-listing.html',
                controller: 'clientlistingCtrl',
            })
    
            .state('add-client', {
                url: '/add-client',
                templateUrl: 'templates/add-client.html',
                controller: 'addclientCtrl',
            })
    
            .state('user-listing', {
                url: '/user-listing',
                templateUrl: 'templates/user-listing.html',
                controller: 'userlistingCtrl',
            })
    
            .state('add-user', {
                url: '/add-user',
                templateUrl: 'templates/add-user.html',
                controller: 'adduserCtrl',
            })
    
            .state('project-type', {
                url: '/project-type',
                templateUrl: 'templates/project-type.html',
                controller: 'projecttypeCtrl',
            })
    
            .state('project-listing', {
                url: '/project-listing',
                templateUrl: 'templates/project-listing.html',
                controller: 'projectlistingCtrl',
            })
    
            .state('add-project', {
                url: '/add-project',
                templateUrl: 'templates/add-project.html',
                controller: 'addprojectCtrl',
            })
    
            .state('equipment-selection', {
                url: '/equipment-selection',
                templateUrl: 'templates/equipment-selection.html',
                controller: 'equipmentselectionCtrl',
            })
    
            .state('material-movement', {
                url: '/material-movement',
                templateUrl: 'templates/material-movement.html',
                controller: 'materialmovementCtrl',
            })
    
            .state('project-consumption', {
                url: '/project-consumption',
                templateUrl: 'templates/project-consumption.html',
                controller: 'projectconsumptionCtrl',
            })
    
            .state('project-dashboard', {
                url: '/project-dashboard',
                templateUrl: 'templates/project-dashboard.html',
                controller: 'projectdashboardCtrl',
            })

        $urlRouterProvider.otherwise('/login');
    })
