var app = angular.module("starter.controllers", []);

app.controller('loginCtrl', function ($scope) {
    console.log("loginCtrl called");
});

app.controller('actionsCtrl', function ($scope) {
    console.log("actionsCtrl called");
});

app.controller('clientlistingCtrl', function ($scope) {
    console.log("clientlistingCtrl called");
});

app.controller('addclientCtrl', function ($scope) {
    console.log("addclientCtrl called");
});

app.controller('userlistingCtrl', function ($scope) {
    console.log("userlistingCtrl called");
});

app.controller('adduserCtrl', function ($scope) {
    console.log("adduserCtrl called");
});

app.controller('projecttypeCtrl', function ($scope) {
    console.log("projecttypeCtrl called");
});

app.controller('projectlistingCtrl', function ($scope) {
    console.log("projectlistingCtrl called");
});

app.controller('addprojectCtrl', function ($scope) {
    console.log("addprojectCtrl called");
});

app.controller('equipmentselectionCtrl', function ($scope) {
    console.log("equipmentselectionCtrl called");
});

app.controller('materialmovementCtrl', function ($scope) {
    console.log("materialmovementCtrl called");
});

app.controller('projectconsumptionCtrl', function ($scope) {
    console.log("projectconsumptionCtrl called");
});

app.controller('projectdashboardCtrl', function ($scope) {
    console.log("projectdashboardCtrl called");
});
